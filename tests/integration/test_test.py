from base64 import b64decode
import os
from time import sleep

import pytest

from bmb_martlet_organization_client import (  # type: ignore
    ApiClient,
    Configuration,
)
from bmbix_sdk.api import (
    build_token_manager,
)
from bmbix_sdk.documents.ubl_2_2.invoice import (
    dump_xml,
)


@pytest.fixture
def client_secret():
    return os.environ["BMBIX_CLIENT_SECRET"]


@pytest.fixture
def client_id():
    return os.environ["BMBIX_CLIENT_ID"]


@pytest.fixture
def sender_bmbix_address():
    return os.environ["BMBIX_SENDER"]


@pytest.fixture
def bmbix_destination_address():
    return os.environ["BMBIX_RECIPIENT"]


@pytest.fixture
def organization_id():
    return os.environ["BMBIX_ORGANIZATION"]


@pytest.mark.skip()
def test_addressed_message(
    invoice,
    client_id,
    client_secret,
    organization_id,
    sender_bmbix_address,
    bmbix_destination_address,
):
    token_url = "https://auth2.bmbix.com/oauth2/token"
    content_media_type = "ubl-invoice-2-2"
    reference = "INV0000"

    token_manager = build_token_manager(
        client_id=client_id,
        client_secret=client_secret,
        token_url=token_url,
    )

    configuration = Configuration()
    token = token_manager()
    configuration.access_token = token["access_token"]
    api_client = ApiClient(configuration)
    messages_api = MessagesApi(api_client)

    submission = submit_message(
        token_manager=token_manager,
        sender_bmbix_address=sender_bmbix_address,
        reference=reference,
        document=dump_xml(invoice.xml()),
        bmbix_destination_address=bmbix_destination_address,
        content_media_type=content_media_type,
    )
    print(submission)

    sleep(3)

    received_message_response = messages_api.select_received_message(
        organization_id=organization_id,
        address_id=sender_bmbix_address,
        message_id=submission.message_id,
    )
    received_message = received_message_response.received_message

    message_response = messages_api.select_no_context(
        message_id=submission.message_id,
    )
    message = message_response.message
    print(b64decode(message.content))

    assert received_message.status == "accepted"

    message2 = messages_api.select(
        organization_id,
        bmbix_destination_address,
        submission.message_id,
    ).message

    assert message2.content == message.content


def test_unaddressed_message(
    invoice,
    client_secret,
    client_id,
    sender_bmbix_address,
):
    token_url = "https://auth2.bmbix.com/oauth2/token"
    content_media_type = "ubl-invoice-2-2"
    reference = "INV9999"
    local_account = "ACC000"
    summary = "here is the summary"

    token_manager = build_token_manager(
        client_id=client_id,
        client_secret=client_secret,
        token_url=token_url,
    )

    configuration = Configuration()
    token = token_manager()
    configuration.access_token = token["access_token"]

    unaddressed_message = submit_unaddressed_message(
        token_manager=token_manager,
        sender_bmbix_address=sender_bmbix_address,
        reference=reference,
        document=dump_xml(invoice.xml()),
        content_media_type=content_media_type,
        local_account=local_account,
        summary=summary,
    )
    print(unaddressed_message)
    assert unaddressed_message  # == 0
