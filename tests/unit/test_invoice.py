from bmbix_sdk.documents.ubl_2_2.invoice import (
    dump_xml,
)


def test(invoice):
    assert dump_xml(invoice.xml())
