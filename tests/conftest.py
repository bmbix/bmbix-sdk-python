from datetime import datetime
from uuid import (
    uuid4,
)

import pytest

from bmbix_sdk.documents.ubl_2_2.invoice import (
    dump_xml,
    AccountingCustomerParty,
    AccountingSupplierParty,
    Invoice,
    InvoiceLine,
    Item,
    LegalMonetaryTotal,
    Party,
    PartyLegalEntity,
    PartyTaxScheme,
    PayableAmount,
    PostalAddress,
    TaxTotal,
)


@pytest.fixture
def invoice():
    id_ = uuid4()
    issue_date = "2022-01-19"
    accounting_supplier_party = AccountingSupplierParty(party=Party(
        party_id="some party id",
        party_name="Dave the Supplier",
        postal_address=PostalAddress(
            street_name="High Street",
            building_name="",
            building_number="43",
            city_name="Gotham",
            postal_zone="SW1 1AA",
            country_subentity="Greater Gotham",
            address_line="here be some address line",
            country_identification_code="GB",
        ),
        party_tax_scheme=PartyTaxScheme(
            registration_name="",
            company_id="",
        ),
        party_legal_entity=PartyLegalEntity(
            registration_name="",
            company_id="",
        ),
    ))
    accounting_customer_party = AccountingCustomerParty(party=Party(
        party_id="some other party id",
        party_name="Sarah the Customer",
        postal_address=PostalAddress(
            street_name="Main Avenue",
            building_name="",
            building_number="9",
            city_name="Liverpool",
            postal_zone="L9 42JJ",
            country_subentity="Lancashire",
            address_line="here be some other address line",
            country_identification_code="GB",
        ),
        party_tax_scheme=PartyTaxScheme(
            registration_name="",
            company_id="",
        ),
        party_legal_entity=PartyLegalEntity(
            registration_name="",
            company_id="",
        ),
    ))
    legal_monetary_total = LegalMonetaryTotal(
        payable_amount=PayableAmount(
            amount="10.00",
            currency_id="GBP",
        ),
        allowance_total_amount="2.00",
        line_extension_amount="8.00",
        tax_exclusive_amount="4.00",
    )
    invoice_line1 = InvoiceLine(
        id=23923,
        price="7.48",
        invoiced_quantity="32.0",
        unit_of_measure="unit",
        line_extension_amount="8.00",
        item=Item(
            descriptions=["this is an item description"],
            sellers_item_identification="some sellers identification",

        ),
    )
    invoice_lines = [
        invoice_line1,
    ]
    tax_total = TaxTotal(
        tax_type="Some tax type",
        tax_amount="3220.99",
    )
    i = Invoice(
        id=id_,
        issue_date=issue_date,
        accounting_supplier_party=accounting_supplier_party,
        accounting_customer_party=accounting_customer_party,
        legal_monetary_total=legal_monetary_total,
        invoice_lines=invoice_lines,
        tax_total=tax_total,
    )
    return i
