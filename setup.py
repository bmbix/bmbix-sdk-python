from setuptools import setup, find_packages

install_requires = [
    "attrs",
    "cryptography",
    "dateparser",
    "lxml",
    "oauthlib",
    "requests-oauthlib",
    "pycryptodome",
    "bmb-klondike-client",
    "bmb-martlet-organization-client",
]
EXTRAS_REQUIRE = {
    "tests": [
        "pytest",
    ],
}

setup(
    name="bmbix-sdk",
    url="https://gitlab.com/bmbix/bmbix-sdk-python",
    author="Robin Abbi",
    author_email="robin.abbi@bmbix.com",
    description="Bmbix SDK",
    long_description="Interact with Bmbix structured messaging service",
    license="Apache",
    keywords="Bmbix, SDK",
    install_requires=install_requires,
    extras_require=EXTRAS_REQUIRE,
    use_scm_version={
        "write_to": "src/bmbix_sdk/version.py",
    },
    setup_requires=["setuptools_scm"],
    package_dir={"": "src"},
    packages=find_packages(where="src"),
)
