# Bmbix SDK

Bmbix is a structured message service. Use it to transfer business messages between or within organizations.

## Getting started

### Prerequisites

Sign up for an account on `bmbix.com <https://bmbix.com>`

You will need to create an organization and an address for at least one business process such as sales.

In the bmbix client app, go to Manage > ${your organization} > Access Keys where you will find your Oauth2 client credentials.


### Installation

```shell
pip install bmbix-sdk
```


### Invocation

```python

import os

from bmbix_sdk.api import build_token_manager, submit_message

token_manager = build_token_manager(
    token_url="https://auth2.bmbix.com/oauth2/token",
    client_id=os.environ["BMBIX_CLIENT_ID"],
    client_secret=os.environ["BMBIX_CLIENT_SECRET"],
)

sender_bmbix_address = "b5904fb4-0ad1-436c-9569-5893861c880b"
reference = "INV00001"
with open("peppol_invoice.xml", "r") as fh:
    document = fh.read()
bmbix_destination_address = "7fe3fdf1-49fc-404b-9767-2cf958862dc1"
content_media_type = "peppol-bis-invoice-3"

submission = submit_message(
    token_manager=token_manager,
    sender_bmbix_address=sender_bmbix_address,
    reference=reference,
    document=document,
    bmbix_destination_address=bmbix_destination_address,
    content_media_type=content_media_type,
)

print(submission)

```

## Help

If you need any help, reach us on  [bmbix-help](https://discord.gg/RqnaKDVMqg) on Discord.
